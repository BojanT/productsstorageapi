﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Http;
using System.Web.Http.Results;
using System.Collections.Generic;
using System.Linq;
using Moq;
using StorageApi.Controllers;
using StorageApi.Interfaces;
using StorageApi.Models;

namespace StorageApiUnitTests.StorageApiTests
{
	[TestClass]
	public class ProductsControllerTests
	{
		private readonly Mock<IProductRepository> mockRepo = new Mock<IProductRepository>();

		#region [ TestMethodsForGetAll ]

		[TestMethod]
		public void GetReturnsMultipleObjects()
		{
			// Arange
			List<ProductDTO> products = new List<ProductDTO>();
			products.Add(new ProductDTO { Id = 1, Name = "ProductOne" });
			products.Add(new ProductDTO { Id = 2, Name = "ProductTwo" });

			mockRepo.Setup(x => x.GetAll()).Returns(products.AsEnumerable());
			var controller = new ProductsController(mockRepo.Object);

			// Act
			IEnumerable<ProductDTO> result = controller.Get();

			// Assert
			Assert.IsNotNull(result);
			Assert.AreEqual(products.Count, result.ToList().Count);
			Assert.AreEqual(products.ElementAt(0), result.ElementAt(0));
			Assert.AreEqual(products.ElementAt(1), result.ElementAt(1));
		}

		#endregion [ TestMethodsForGetAll ]

		#region [ TestMethodsForGetById ]

		[TestMethod]
		public void GetReturnsOkAndCategoryById()
		{
			// Arange
			mockRepo.Setup(x => x.GetById(1)).Returns(new ProductDetailsDTO { Id = 1, Name = "ProductOne", Price = 1200 });
			var controller = new ProductsController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Get(1);
			var contentResult = actionResult as OkNegotiatedContentResult<ProductDetailsDTO>;

			// Assert
			Assert.IsNotNull(contentResult);
			Assert.IsNotNull(contentResult.Content);
			Assert.AreEqual(1, contentResult.Content.Id);
		}

		[TestMethod]
		public void GetCategoryByIdReturnsNotFound()
		{
			// Arange
			var controller = new ProductsController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Get(22);

			// Assert
			Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
		}

		#endregion [ TestMethodsForGetById ]

		#region [ TestMethodsForGetWithLowestPrice ]

		[TestMethod]
		public void GetReturnsTwoProductsWithLowestPrices()
		{
			// Arrange 
			List<Product> products = new List<Product>();
			products.Add(new Product { Id = 1, Name = "ProductOne" });
			products.Add(new Product { Id = 2, Name = "ProductTwo" });

			mockRepo.Setup(x => x.ShowProductsWithLowestPrices()).Returns(products);
			var controller = new ProductsController(mockRepo.Object);

			// Act
			IEnumerable<Product> result = controller.GetWithLowestPrice();

			// Assert
			Assert.IsNotNull(result);
			Assert.AreEqual(result.ToList().Count, 2);
		}

		#endregion [ TestMethodsForGetWithLowestPrice ]

		#region [ TestMethodsForPostReturnsProducts ]

		[TestMethod]
		public void PostReturnsAllProductsFromOneCategory()
		{
			// Arrange
			List<Product> products = new List<Product>();
			products.Add(new Product { Id = 1, Name = "ProductOne", CategoryId = 1 });
			products.Add(new Product { Id = 2, Name = "ProductTwo", CategoryId = 1 });
			Category category = new Category { Id = 1, Name = "CategoryOne"};

			mockRepo.Setup(x => x.GetByCategory(category)).Returns(products);
			var controller = new ProductsController(mockRepo.Object);

			// Act
			IEnumerable<Product> result = controller.Post(category);

			// Assert
			Assert.IsNotNull(result);
			Assert.AreEqual(products.Count(), result.ToList().Count);
			Assert.AreEqual(result.ToList()[0].CategoryId, 1);
		}

		#endregion [ TestMethodsForPostReturnsProducts ]

		#region [ TestMethodsForPostReturnsProductsByPrice ]

		[TestMethod]
		public void PostReturnsProductsWithLowerPriceThenEnteredPrice()
		{
			// Arrange
			List<Product> products = new List<Product>();
			products.Add(new Product { Id = 1, Name = "ProductOne", Price = 250 });
			products.Add(new Product { Id = 2, Name = "ProductTwo", Price = 350 });

			mockRepo.Setup(x => x.GetByPrice(450)).Returns(products);
			var controller = new ProductsController(mockRepo.Object);

			// Act
			IEnumerable<Product> result = controller.Post(450);

			// Assert
			Assert.IsNotNull(result);
			Assert.AreEqual(products.Count, result.ToList().Count);
		}

		#endregion [ TestMethodsForPostReturnsProductsByPrice ]

		#region [ TestMethodsForPost ]

		[TestMethod]
		public void PostReturnsOkAndNewCategory()
		{
			// Arrange
			Product product = new Product { Id = 1, Name = "ProductOne", Price = 1200 };
			mockRepo.Setup(x => x.Add(product)).Returns(true);
			var controller = new ProductsController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Post(product);
			var contentResult = actionResult as OkNegotiatedContentResult<Product>;

			// Assert
			Assert.IsNotNull(contentResult);
			Assert.IsNotNull(contentResult.Content);
			Assert.AreEqual(contentResult.Content.Id, 1);
		}

		[TestMethod]
		public void PostReturnsBadRequestWhenAddingNewCategoryFails()
		{
			// Arange
			mockRepo.Setup(x => x.Add(new Product { Id = 1, Name = "ProductOne" })).Returns(false);
			var controller = new ProductsController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Post(new Product { Id = 1, Name = "ProductOne" });

			// Assert
			Assert.IsNotNull(actionResult);
			Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
		}

		[TestMethod]
		public void PostReturnsBadRequestWhenModelStateIsNotValid()
		{
			// Arange
			var controller = new ProductsController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Post(new Product { Id = 1, Name = null });

			// Assert
			Assert.IsNotNull(actionResult);
			Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
		}

		[TestMethod]
		public void PostReturnsBadRequestWhenParameterIsNull()
		{
			// Arange
			Product product = null;
			var controller = new ProductsController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Post(product);

			// Assert
			Assert.IsNotNull(actionResult);
			Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
		}

		#endregion [ TestMethodsForPost ]

		#region [ TestMethodsForPut ]

		[TestMethod]
		public void PutReturnsOkAndCategoryObject()
		{
			// Arrange
			Product product = new Product { Id = 1, Name = "ProductOne", Price = 1200 };
			mockRepo.Setup(x => x.Update(product)).Returns(true);
			var controller = new ProductsController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Put(product.Id, product);
			var contentResult = actionResult as OkNegotiatedContentResult<Product>;

			// Assert
			Assert.IsNotNull(contentResult);
			Assert.IsNotNull(contentResult.Content);
			Assert.IsInstanceOfType(contentResult, typeof(OkNegotiatedContentResult<Product>));
		}

		[TestMethod]
		public void PutReturnsBadRequestWhenUpdateFails()
		{
			// Arrange
			mockRepo.Setup(x => x.Update(new Product { Id = 1, Name = "ProductOne" })).Returns(false);
			var controller = new ProductsController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Put(1, new Product { Id = 1, Name = "ProductOne" });

			// Assert
			Assert.IsNotNull(actionResult);
			Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
		}

		[TestMethod]
		public void PutReturnsBadRequestWhenModelStateIsNotValid()
		{
			// Arrange 
			var controller = new ProductsController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Put(1, new Product { Id = 1, Name = null });

			// Assert
			Assert.IsNotNull(actionResult);
			Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
		}

		[TestMethod]
		public void PutReturnsBadRequestIfIdIsNotMatch()
		{
			// Arrange
			Product product = new Product { Id = 1, Name = "ProductOne" };
			var controller = new ProductsController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Put(12, product);

			// Assert
			Assert.AreNotEqual(12, product.Id);
			Assert.IsNotNull(actionResult);
			Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
		}

		[TestMethod]
		public void PutReturnsBadRequestWhenCategoryIsNull()
		{
			// Arrange
			var controller = new ProductsController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Put(1, null);

			// Assert
			Assert.IsNotNull(actionResult);
			Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
		}

		#endregion [ TestMethodsForPut ]

		#region [ TestMethodsForDelete ]

		[TestMethod]
		public void DeleteReturnsOk()
		{
			// Arrange
			ProductDetailsDTO product = new ProductDetailsDTO { Id = 1, Name = "ProductOne" };
			mockRepo.Setup(x => x.GetById(1)).Returns(product);
			mockRepo.Setup(x => x.Delete(1)).Returns(true);
			var controller = new ProductsController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Delete(1);

			// Assert
			Assert.IsNotNull(actionResult);
			Assert.IsInstanceOfType(actionResult, typeof(OkResult));
		}

		[TestMethod]
		public void DeleteReturnsBadRequestIfDeletingFails()
		{
			// Arrange
			mockRepo.Setup(x => x.Delete(1)).Returns(false);
			var controller = new ProductsController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Delete(1);

			// Assert
			Assert.IsNotNull(actionResult);
			Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
		}

		#endregion [ TestMethodsForDelete ]
	}
}
