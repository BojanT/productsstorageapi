﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Http;
using System.Web.Http.Results;
using System.Collections.Generic;
using System.Linq;
using StorageApi.Interfaces;
using StorageApi.Models;
using StorageApi.Controllers;
using Moq;

namespace StorageApiUnitTests
{
	[TestClass]
	public class CategoriesControllerTests
	{
		private readonly Mock<ICategoryRepository> mockRepo = new Mock<ICategoryRepository>();
		private readonly Category categoryOne = new Category { Id = 1, Name = "CategoryOne" };
		private readonly Category categoryTwo = new Category { Id = 2, Name = "CategoryTwo" };
		private readonly List<Category> categories = new List<Category>();

		#region [ TestMethodsForGetAll ]

		[TestMethod]
		public void GetReturnsMultipleObjects()
		{
			// Arange
			categories.Add(categoryOne);
			categories.Add(categoryTwo);

			mockRepo.Setup(x => x.GetAll()).Returns(categories.AsEnumerable());
			var controller = new CategoriesController(mockRepo.Object);

			// Act
			IEnumerable<Category> result = controller.Get();

			// Assert
			Assert.IsNotNull(result);
			Assert.AreEqual(categories.Count, result.ToList().Count);
			Assert.AreEqual(categories.ElementAt(0), result.ElementAt(0));
			Assert.AreEqual(categories.ElementAt(1), result.ElementAt(1));
		}

		#endregion [ TestMethodsForGetAll ]

		#region [ TestMethodsForGetById ]

		[TestMethod]
		public void GetReturnsOkAndCategoryById()
		{
			// Arange
			mockRepo.Setup(x => x.GetById(1)).Returns(categoryOne);
			var controller = new CategoriesController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Get(1);
			var contentResult = actionResult as OkNegotiatedContentResult<Category>;

			// Assert
			Assert.IsNotNull(contentResult);
			Assert.IsNotNull(contentResult.Content);
			Assert.AreEqual(1, contentResult.Content.Id);
		}

		[TestMethod]
		public void GetCategoryByIdReturnsNotFound()
		{
			// Arange
			var controller = new CategoriesController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Get(22);

			// Assert
			Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
		}

		#endregion [ TestMethodsForGetById ]

		#region [ TestMethodsForGetStatistics ]

		[TestMethod]
		public void GetReturnsTwoObjects()
		{
			// Arrange
			List<CategoryPrice> categories = new List<CategoryPrice>();
			categories.Add(new CategoryPrice { Category = new Category(), PriceSum = 1 });
			categories.Add(new CategoryPrice { Category = new Category(), PriceSum = 1 });

			mockRepo.Setup(x => x.GetStatistics()).Returns(categories.AsEnumerable());
			var controller = new CategoriesController(mockRepo.Object);

			// Act
			IEnumerable<CategoryPrice> result = controller.GetStatistics();

			// Assert
			Assert.IsNotNull(result);
			Assert.AreEqual(result.ToList().Count, 2);
		}

		#endregion [ TestMethodsForGetTwoCategories ]

		#region [ TestMethodsForPost ]

		[TestMethod]
		public void PostReturnsOkAndNewCategory()
		{
			// Arrange
			mockRepo.Setup(x => x.Add(categoryOne)).Returns(true);
			var controller = new CategoriesController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Post(categoryOne);
			var contentResult = actionResult as OkNegotiatedContentResult<Category>;

			// Assert
			Assert.IsNotNull(contentResult);
			Assert.IsNotNull(contentResult.Content);
			Assert.AreEqual(contentResult.Content.Id, 1);
		}

		[TestMethod]
		public void PostReturnsBadRequestWhenAddingNewCategoryFails()
		{
			// Arange
			mockRepo.Setup(x => x.Add(categoryOne)).Returns(false);
			var controller = new CategoriesController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Post(categoryOne);

			// Assert
			Assert.IsNotNull(actionResult);
			Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
		}

		[TestMethod]
		public void PostReturnsBadRequestWhenModelStateIsNotValid()
		{
			// Arange
			var controller = new CategoriesController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Post(new Category());

			// Assert
			Assert.IsNotNull(actionResult);
			Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
		}

		[TestMethod]
		public void PostReturnsBadRequestWhenParameterIsNull()
		{
			// Arange
			var controller = new CategoriesController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Post(null);

			// Assert
			Assert.IsNotNull(actionResult);
			Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
		}

		#endregion [ TestMethodsForPost ]

		#region [ TestMethodsForPut ]

		[TestMethod]
		public void PutReturnsOkAndCategoryObject()
		{
			// Arrange
			mockRepo.Setup(x => x.Update(categoryOne)).Returns(true);
			var controller = new CategoriesController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Put(categoryOne.Id, categoryOne);
			var contentResult = actionResult as OkNegotiatedContentResult<Category>;

			// Assert
			Assert.IsNotNull(contentResult);
			Assert.IsNotNull(contentResult.Content);
			Assert.IsInstanceOfType(contentResult, typeof(OkNegotiatedContentResult<Category>));
		}

		[TestMethod]
		public void PutReturnsBadRequestWhenUpdateFails()
		{
			// Arrange
			mockRepo.Setup(x => x.Update(categoryOne)).Returns(false);
			var controller = new CategoriesController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Put(categoryOne.Id, categoryOne);

			// Assert
			Assert.IsNotNull(actionResult);
			Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
		}

		[TestMethod]
		public void PutReturnsBadRequestWhenModelStateIsNotValid()
		{
			// Arrange 
			var controller = new CategoriesController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Put(categoryOne.Id, new Category { Id = 1 });

			// Assert
			Assert.IsNotNull(actionResult);
			Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
		}

		[TestMethod]
		public void PutReturnsBadRequestIfIdIsNotMatch()
		{
			// Arrange
			var controller = new CategoriesController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Put(12, categoryTwo);

			// Assert
			Assert.AreNotEqual(12, categoryTwo.Id);
			Assert.IsNotNull(actionResult);
			Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
		}

		[TestMethod]
		public void PutReturnsBadRequestWhenCategoryIsNull()
		{
			// Arrange
			var controller = new CategoriesController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Put(categoryOne.Id, null);

			// Assert
			Assert.IsNotNull(actionResult);
			Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
		}

		#endregion [ TestMethodsForPut ]

		#region [ TestMethodsForDelete ]

		[TestMethod]
		public void DeleteReturnsOk()
		{
			// Arrange
			mockRepo.Setup(x => x.GetById(categoryOne.Id)).Returns(categoryOne);
			mockRepo.Setup(x => x.Delete(categoryOne)).Returns(true);
			var controller = new CategoriesController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Delete(categoryOne.Id);

			// Assert
			Assert.IsNotNull(actionResult);
			Assert.IsInstanceOfType(actionResult, typeof(OkResult));
		}

		[TestMethod]
		public void DeleteReturnsBadRequestIfDeletingFails()
		{
			// Arrange
			mockRepo.Setup(x => x.GetById(categoryOne.Id)).Returns(categoryOne);
			mockRepo.Setup(x => x.Delete(categoryOne)).Returns(false);
			var controller = new CategoriesController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Delete(categoryOne.Id);

			// Assert
			Assert.IsNotNull(actionResult);
			Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
		}

		[TestMethod]
		public void DeleteReturnsNotFoundIfCategoryIsNotFound()
		{
			// Arrange
			var controller = new CategoriesController(mockRepo.Object);

			// Act
			IHttpActionResult actionResult = controller.Delete(12);

			// Assert
			Assert.IsNotNull(actionResult);
			Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
		}

		#endregion [ TestMethodsForDelete ]
	}
}
