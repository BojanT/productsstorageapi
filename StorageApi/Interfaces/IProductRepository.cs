﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StorageApi.Models;

namespace StorageApi.Interfaces
{
	public interface IProductRepository
	{
		IEnumerable<ProductDTO> GetAll();
		IEnumerable<Product> GetByCategory(Category category);
		IEnumerable<Product> GetByPrice(int price);
		ProductDetailsDTO GetById(int id);
		IEnumerable<Product> ShowProductsWithLowestPrices();
		bool Exists(int id);
		bool Add(Product product);
		bool Update(Product product);
		bool Delete(int id);
	}
}
