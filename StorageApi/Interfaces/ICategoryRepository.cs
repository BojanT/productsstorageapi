﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StorageApi.Models;

namespace StorageApi.Interfaces
{
	public interface ICategoryRepository
	{
		IEnumerable<Category> GetAll();
		IEnumerable<CategoryPrice> GetStatistics();
		Category GetById(int id);
		bool Exists(int id);
		bool Add(Category newCategory);
		bool Update(Category category);
		bool Delete(Category category);
	}
}
