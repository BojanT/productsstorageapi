﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using StorageApi.Models;
using StorageApi.Interfaces;

namespace StorageApi.Controllers
{
	[Authorize]
    public class ProductsController : ApiController
    {
		private IProductRepository _repository;

		public ProductsController(IProductRepository repository)
		{
			_repository = repository ?? throw new ArgumentNullException(nameof(repository));
		}

		// GET api/products
		[AllowAnonymous]
		public IEnumerable<ProductDTO> Get()
		{
			return _repository.GetAll();
		}

		// GET api/products/id
		[AllowAnonymous]
		public IHttpActionResult Get(int id)
		{
			try
			{
				var product = _repository.GetById(id);

				if (product == null)
				{
					return NotFound();
				}

				return Ok(product);
			}
			catch (Exception)
			{
				return StatusCode(HttpStatusCode.BadRequest);
			}
		}

		// GET api/lowest
		[Route("api/lowest")]
		[AllowAnonymous]
		public IEnumerable<Product> GetWithLowestPrice()
		{
			return _repository.ShowProductsWithLowestPrices();
		}

		// POST api/GetByCategory
		[Route("api/getByCategory")]
		[AllowAnonymous]
		public IEnumerable<Product> Post(Category category)
		{
			return _repository.GetByCategory(category);
		}

		// POST api/products?price=250
		[AllowAnonymous]
		public IEnumerable<Product> Post([FromUri]int price)
		{
			return _repository.GetByPrice(price);
		}

		// POST api/products
		[AllowAnonymous]
		public IHttpActionResult Post(Product product)
		{
			try
			{
				if (product == null)
				{
					return BadRequest();
				}

				if (!ModelState.IsValid)
				{
					return BadRequest(ModelState);
				}

				if (!_repository.Add(product))
				{
					return BadRequest();
				}

				return Ok(product);
			}
			catch (Exception)
			{
				return StatusCode(HttpStatusCode.BadRequest);
			}
		}

		// PUT api/products/id
		public IHttpActionResult Put(int id, Product product)
		{
			try
			{
				if (product == null)
				{
					return BadRequest();
				}

				if (id != product.Id)
				{
					return BadRequest();
				}

				if (!ModelState.IsValid)
				{
					return BadRequest(ModelState);
				}

				if (!_repository.Update(product))
				{
					return BadRequest();
				}

				return Ok(product);
			}
			catch (Exception)
			{
				return StatusCode(HttpStatusCode.BadRequest);
			}
		}

		// DELETE api/products/id
		public IHttpActionResult Delete(int id)
		{
			try
			{
				if (!_repository.Delete(id))
				{
					return BadRequest();
				}

				return Ok();
			}
			catch (Exception)
			{
				return StatusCode(HttpStatusCode.BadRequest);
			}
		}
	}
}
