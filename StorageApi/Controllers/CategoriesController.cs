﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using StorageApi.Models;
using StorageApi.Interfaces;

namespace StorageApi.Controllers
{
	[Authorize]
    public class CategoriesController : ApiController
    {
		private ICategoryRepository _repository;

		public CategoriesController(ICategoryRepository repository)
		{
			_repository = repository ?? throw new ArgumentNullException(nameof(repository));
		}

		// GET api/categories
		[AllowAnonymous]
		public IEnumerable<Category> Get()
		{
			return _repository.GetAll();
		}

		// GET api/categories/id
		[AllowAnonymous]
		public IHttpActionResult Get(int id)
		{
			try
			{
				var category = _repository.GetById(id);

				if (category == null)
				{
					return NotFound();
				}
				return Ok(category);
			}
			catch (Exception)
			{
				return StatusCode(HttpStatusCode.BadRequest);
			}
		}

		// GET api/statistics
		[Route("api/statistics")]
		[AllowAnonymous]
		public IEnumerable<CategoryPrice> GetStatistics()
		{
			return _repository.GetStatistics();
		}

		// POST api/categories
		[AllowAnonymous]
		public IHttpActionResult Post(Category category)
		{
			try
			{
				if (category == null)
				{
					return BadRequest();
				}

				if (!ModelState.IsValid)
				{
					return BadRequest(ModelState);
				}

				if (!_repository.Add(category))
				{
					return BadRequest();
				}

				return Ok(category);
			}
			catch (Exception)
			{
				return StatusCode(HttpStatusCode.BadRequest);
			}
		}

		// PUT api/categories/id
		public IHttpActionResult Put(int id, Category category)
		{
			try
			{
				if (category == null)
				{
					return BadRequest();
				}

				if (id != category.Id)
				{
					return BadRequest();
				}

				if (!ModelState.IsValid)
				{
					return BadRequest(ModelState);
				}

				if (!_repository.Update(category))
				{
					return BadRequest();
				}

				return Ok(category);
			}
			catch (Exception)
			{
				return StatusCode(HttpStatusCode.BadRequest);
			}
		}

		// DELETE api/categories/id
		public IHttpActionResult Delete(int id)
		{
			try
			{
				var category = _repository.GetById(id);

				if (category == null)
				{
					return NotFound();
				}

				if (!_repository.Delete(category))
				{
					return BadRequest();
				}

				return Ok();
			}
			catch (Exception)
			{
				return StatusCode(HttpStatusCode.BadRequest);
			}
		}
	}
}
