﻿$(document).ready(function () {

    // data of interest
    var host = "http://localhost:";
    var port = "50251/";
    var token = null;
    var headers = {};
    var accountEndpoint = "api/Account/Register";
    var categoriesEndpoint = "api/categories/";
    var statisticsEndpoint = "api/statistics/";
    var productsEndpoint = "api/products/";
    var lowestEndpoint = "api/lowest/";
    var getByCategoryEndpoint = "api/getByCategory/";
    var formAction = "Create";
    var editingId;


    // LOGIN AND REGISTRATION //////////////////////////////////////////////////////////////////////////////


    // hide logout form
    $("#logout").css("display", "none");

    // user registration
    $("#registration").submit(function (e) {
        e.preventDefault();

        var email = $("#regEmail").val();
        var pass1 = $("#regPass1").val();
        var pass2 = $("#regPass2").val();

        // object for sending
        var sendData = {
            "Email": email,
            "Password": pass1,
            "ConfirmPassword": pass2
        };


        $.ajax({
            type: "POST",
            url: host + port + accountEndpoint,
            data: sendData

        }).done(function (data) {
            $("#regEmail").val('');
            $("#regPass1").val('');
            $("#regPass2").val('');
            $("#info").append("Successfull registration. You can login.");

        }).fail(function (data) {
            alert(data);
        });


    });


    // user login
    $("#login").submit(function (e) {
        e.preventDefault();

        var email = $("#logEmail").val();
        var pass = $("#logPass").val();

        // object for sending
        var sendData = {
            "grant_type": "password",
            "username": email,
            "password": pass
        };

        $.ajax({
            "type": "POST",
            "url": host + port + "Token",
            "data": sendData

        }).done(function (data) {
            console.log(data);
            $("#info").empty().append("Logged in user: " + data.userName);
            token = data.access_token;
            $("#logEmail").val('');
            $("#logPass").val('');
            $("#login").css("display", "none");
            $("#registration").css("display", "none");
            $("#logout").css("display", "block");

        }).fail(function (data) {
            alert(data);
        });
    });

    // user logout
    $("#userLogout").click(function () {
        token = null;
        headers = {};

        $("#login").css("display", "block");
        $("#registration").css("display", "block");
        $("#logout").css("display", "none");
        $("#info").empty();
        //$("#sadrzaj").empty();

    })


    // CATEGORIES ////////////////////////////////////////////////////////////////////////////////////


    // ready event for deleting
    $("body").on("click", "#btnDeleteCat", deleteCategory);

    // ready event for updating
    $("body").on("click", "#btnEditCat", editCategory);

    // display categories
    $("#btnCategories").click(function () {
        var requestUrl = host + port + categoriesEndpoint;
        console.log("URL zahteva: " + requestUrl);
        $.getJSON(requestUrl, setCategories);
    });

    // setting table for categories
    function setCategories(data, status) {
        console.log("Status: " + status);

        var $container = $("#dataCategories");
        $container.empty();

        if (status == "success") {
            console.log(data);
            
            var div = $("<div></div>");
            var h1 = $("<h1>Categories</h1>");
            div.append(h1);
            
            var table = $("<table border=1></table>");
            var header = $("<tr><td>Id</td><td>Name</td><td>Delete</td><td>Edit</td></tr>");
            table.append(header);
            for (i = 0; i < data.length; i++) {
                
                var row = "<tr>";
                var displayData = "<td>" + data[i].Id + "</td><td>" + data[i].Name + "</td>";
                var stringId = data[i].Id.toString();
                var displayDelete = "<td><button id=btnDeleteCat name=" + stringId + ">Delete</button></td>";
                var displayEdit = "<td><button id=btnEditCat name=" + stringId + ">Edit</button></td>";
                row += displayData + displayDelete + displayEdit + "</tr>";
                table.append(row);
            }

            div.append(table);

            // show form
            $("#formDivOne").css("display", "block");

            // printing content
            $container.append(div);
        }
        else {
            var div = $("<div></div>");
            var h1 = $("<h1>Error while reading categories!</h1>");
            div.append(h1);
            $container.append(div);
        }
    };

    // add new category
    $("#categoryForm").submit(function (e) {

        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        e.preventDefault();

        var categoryName = $("#categoryName").val();
        var httpAction;
        var sendData;
        var url;

        // depend of action prepering object
        if (formAction === "Create") {
            httpAction = "POST";
            url = host + port + categoriesEndpoint;
            sendData = {
                "Name": categoryName
            };
            
        }
        else {
            httpAction = "PUT";
            url = host + port + categoriesEndpoint + editingId.toString();
            sendData = {
                "Id": editingId,
                "Name": categoryName
            };
        }


        console.log("Object for sending");
        console.log(sendData);

        $.ajax({
            url: url,
            type: httpAction,
            data: sendData,
            headers: headers
        })
            .done(function (data, status) {
                formAction = "Create";
                refreshCategoriesTable();
            })
            .fail(function (data, status) {
                alert("Error while adding!");
            })

    });

    // deleting category
    function deleteCategory() {
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }
        // geting {id}
        var deleteID = this.name;
        // sending request 
        $.ajax({
            url: host + port + categoriesEndpoint + deleteID.toString(),
            type: "DELETE",
            headers: headers
        })
            .done(function (data, status) {
                refreshCategoriesTable();
            })
            .fail(function (data, status) {
                alert("Error while deleting!");
            });
    };

    // update category
    function editCategory() {
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }
        // geting id
        var editId = this.name;
        // sending request to get category
        $.ajax({
            url: host + port + categoriesEndpoint + editId.toString(),
            type: "GET",
            headers: headers
        })
            .done(function (data, status) {
                $("#categoryName").val(data.Name);
                editingId = data.Id;
                formAction = "Update";
            })
            .fail(function (data, status) {
                formAction = "Create";
                alert("Error while updating!");
            });
    };

    // refresh display of table
    function refreshCategoriesTable() {
        // clear form
        $("#categoryName").val('');
        // refreshing
        $("#btnCategories").trigger("click");
    };

    // display two categories with highest summed price
    $("#btnStatistics").click(function () {
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }
        var requestUrl = host + port + statisticsEndpoint;
        console.log("URL zahteva: " + requestUrl);
        $.getJSON(requestUrl, headers, setCategoriesStat);
    });

    // setting table for statistics
    function setCategoriesStat(data, status) {
        console.log("Status: " + status);

        var $container = $("#dataCategories");
        $container.empty();

        if (status == "success") {
            console.log(data);

            var div = $("<div></div>");
            var h1 = $("<h3>Two categories with highest summed prices</h3>");
            div.append(h1);

            var table = $("<table border=1></table>");
            var header = $("<tr><td>Name</td><td>Price</td></tr>");
            table.append(header);
            for (i = 0; i < data.length; i++) {

                var row = "<tr>";
                var displayData = "<td>" + data[i].Category.Name + "</td><td>" + data[i].PriceSum + "</td>";
                row += displayData + "</tr>";
                table.append(row);
            }

            div.append(table);

            // hide form
            $("#formDivOne").css("display", "none");

            // printing content
            $container.append(div);
        }
        else {
            var div = $("<div></div>");
            var h1 = $("<h1>Error while reading categories!</h1>");
            div.append(h1);
            $container.append(div);
        }
    };


    // PRODUCTS ///////////////////////////////////////////////////////////////////////////////////////


    // ready event for deleting
    $("body").on("click", "#btnDelete", deleteProduct);

    // ready event for updating
    $("body").on("click", "#btnEdit", editProduct);

    // display products
    $("#btnProducts").click(function () {
        var requestUrl = host + port + productsEndpoint;
        console.log("URL zahteva: " + requestUrl);
        $.getJSON(requestUrl, setProducts);
    });

    // setting table for products
    function setProducts(data, status) {
        console.log("Status: " + status);

        var $container = $("#dataProducts");
        $container.empty();

        if (status == "success") {
            console.log(data);

            var div = $("<div></div>");
            var h1 = $("<h1>Products</h1>");
            div.append(h1);

            var table = $("<table border=1></table>");
            var header = $("<tr><td>Id</td><td>Name</td><td>Delete</td><td>Edit</td></tr>");
            table.append(header);
            for (i = 0; i < data.length; i++) {

                var row = "<tr>";
                var displayData = "<td>" + data[i].Id + "</td><td>" + data[i].Name + "</td>";
                var stringId = data[i].Id.toString();
                var displayDelete = "<td><button id=btnDelete name=" + stringId + ">Delete</button></td>";
                var displayEdit = "<td><button id=btnEdit name=" + stringId + ">Edit</button></td>";
                row += displayData + displayDelete + displayEdit + "</tr>";
                table.append(row);
            }

            div.append(table);

            // show form
            $("#formDivTwo").css("display", "block");

            // hide product details form and table
            $("#formDivThree").css("display", "none");
            var $containerDetails = $("#dataProductDetails");
            $containerDetails.empty();

            // printing content
            $container.append(div);
        }
        else {
            var div = $("<div></div>");
            var h1 = $("<h1>Error while reading products!</h1>");
            div.append(h1);
            $container.append(div);
        }
    };

    // creating dropdown list for categories
    $.ajax({
        url: host + port + categoriesEndpoint,
        type: "Get"
    })
        .done(function (data, status) {
            var create = "<label>Category: <select id=productCategory>";
            for (var i = 0; i < data.length; i++) {
                create += "<option value=" + data[i].Id + ">" + data[i].Name + "</option>";
            }
            create += "</select></label>";
            $("#selection").append(create);
        });

    // add new product
    $("#productForm").submit(function (e) {

        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        e.preventDefault();

        var productName = $("#productName").val();
        var productPrice = $("#productPrice").val();
        var productCategory = $("#productCategory").val();
        var httpAction;
        var sendData;
        var url;

        // depend of action prepering object
        if (formAction === "Create") {
            httpAction = "POST";
            url = host + port + productsEndpoint;
            sendData = {
                "Name": productName,
                "Price": productPrice,
                "CategoryId": productCategory
            };
        }
        else {
            httpAction = "PUT";
            url = host + port + productsEndpoint + editingId.toString();
            sendData = {
                "Id": editingId,
                "Name": productName,
                "Price": productPrice,
                "CategoryId": productCategory
            };
        }

        console.log("Object for sending");
        console.log(sendData);

        $.ajax({
            url: url,
            type: httpAction,
            data: sendData,
            headers: headers
        })
            .done(function (data, status) {
                formAction = "Create";
                refreshProductsTable();
            })
            .fail(function (data, status) {
                alert("Error while adding!");
            })

    });

    // display product details form
    $("#btnProductDetails").click(function () {
        // hide products table
        var $containerData = $("#dataProducts");
        $containerData.empty();
        // hide add form
        $("#formDivTwo").css("display", "none");
        // show details form
        $("#formDivThree").css("display", "block");
    });

    // Form for entering id of product
    $("#productDetailsForm").submit(function (e) {

        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        e.preventDefault();

        var productId = $("#productId").val();

        $.ajax({
            url: host + port + productsEndpoint + productId.toString(),
            type: "Get",
            headers: headers
        })
            .done(function (data, status) {
                setProductDetails(data, status);
            });
    });

    // setting table for product details
    function setProductDetails(data, status) {
        console.log("Status: " + status);

        var $container = $("#dataProductDetails");
        $container.empty();

        if (status == "success") {
            console.log(data);

            var div = $("<div></div>");
            var h1 = $("<h1>Product details</h1>");
            div.append(h1);

            var table = $("<table border=1></table>");
            var header = $("<tr><td>Id</td><td>Name</td><td>Price</td><td>Category</td></tr>");
            table.append(header);

            var row = "<tr>";
            var displayData = "<td>" + data.Id + "</td><td>" + data.Name + "</td><td>" + data.Price + "</td><td>" + data.CategoryName + "</td>";
            row += displayData + "</tr>";
            table.append(row);

            div.append(table);

            // clear product details form
            $("#productId").val('');

            // printing content
            $container.append(div);
        }
        else {
            var div = $("<div></div>");
            var h1 = $("<h1>Error while reading product details!</h1>");
            div.append(h1);
            $container.append(div);
        }
    };

    // deleting product
    function deleteProduct() {
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }
        // geting {id}
        var deleteID = this.name;
        // sending request 
        $.ajax({
            url: host + port + productsEndpoint + deleteID.toString(),
            type: "DELETE",
            headers: headers
        })
            .done(function (data, status) {
                refreshProductsTable();
            })
            .fail(function (data, status) { 
                alert("Error while deleting!");
            });
    };

    // update product
    function editProduct() {
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }
        // geting id
        var editId = this.name;
        // sending request to get category
        $.ajax({
            url: host + port + productsEndpoint + editId.toString(),
            type: "GET",
            headers: headers
        })
            .done(function (data, status) {
                $("#productName").val(data.Name);
                $("#productPrice").val(data.Price);
                $("#productCategory").val(data.CategoryName);
                editingId = data.Id;
                formAction = "Update";
            })
            .fail(function (data, status) {
                formAction = "Create";
                alert("Error while updating!");
            });
    };

    // refresh display of table
    function refreshProductsTable() {
        // clear form
        $("#productName").val('');
        $("#productPrice").val('');
        $("#productCategory").val('');
        // refreshing
        $("#btnProducts").trigger("click");
    };

    // display products
    $("#btnLowest").click(function () {
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }
        var requestUrl = host + port + lowestEndpoint;
        console.log("URL zahteva: " + requestUrl);
        $.getJSON(requestUrl, headers, setLowestProducts);
    });

    // setting table for products
    function setLowestProducts(data, status) {
        console.log("Status: " + status);

        var $container = $("#dataProducts");
        $container.empty();

        if (status == "success") {
            console.log(data);

            var div = $("<div></div>");
            var h1 = $("<h1>Products with lowest prices</h1>");
            div.append(h1);

            var table = $("<table border=1></table>");
            var header = $("<tr><td>Id</td><td>Name</td><td>Price</td></tr>");
            table.append(header);
            for (i = 0; i < data.length; i++) {

                var row = "<tr>";
                var displayData = "<td>" + data[i].Id + "</td><td>" + data[i].Name + "</td><td>" + data[i].Price + "</td>";
                row += displayData + "</tr>";
                table.append(row);
            }

            div.append(table);

            // show form
            $("#formDivTwo").css("display", "none");

            // hide product details form and table
            $("#formDivThree").css("display", "none");
            var $containerDetails = $("#dataProductDetails");
            $containerDetails.empty();

            // printing content
            $container.append(div);
        }
        else {
            var div = $("<div></div>");
            var h1 = $("<h1>Error while reading products!</h1>");
            div.append(h1);
            $container.append(div);
        }
    };

    // display form for entering search price
    $("#btnLowerPrices").click(function () {
        // hide products table
        var $containerData = $("#dataProducts");
        $containerData.empty();
        // hide add form
        $("#formDivTwo").css("display", "none");
        // hide details form
        $("#formDivThree").css("display", "none");
        // show form for entering price for search
        $("#formDivFour").css("display", "block");
    });

    // Form for entering price for search
    $("#lowerPricesForm").submit(function (e) {

        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        e.preventDefault();

        var searchPrice = $("#searchPrice").val();

        $.ajax({
            url: host + port + "api/products?price=" + searchPrice.toString(),
            type: "Post",
            headers: headers
        })
            .done(function (data, status) {
                setLowerPricesProducts(data, status);
            });
    });

    // setting table for product with low prices
    function setLowerPricesProducts(data, status) {
        console.log("Status: " + status);

        var $container = $("#dataLowerPrices");
        $container.empty();

        if (status == "success") {
            console.log(data);

            var div = $("<div></div>");
            var h1 = $("<h3>Products with lower prices then entered price</h3>");
            div.append(h1);

            var table = $("<table border=1></table>");
            var header = $("<tr><td>Id</td><td>Name</td><td>Price</td></tr>");
            table.append(header);

            for (var j = 0; j < data.length; j++) {
                var row = "<tr>";
                var displayData = "<td>" + data[j].Id + "</td><td>" + data[j].Name + "</td><td>" + data[j].Price + "</td>";
                row += displayData + "</tr>";
                table.append(row);
            }

            div.append(table);

            // clear search form
            $("#searchPrice").val('');

            // printing content
            $container.append(div);
        }
        else {
            var div = $("<div></div>");
            var h1 = $("<h1>Error while reading products with lower prices!</h1>");
            div.append(h1);
            $container.append(div);
        }
    };

    // display form for chosing category
    $("#btnProductsFromCat").click(function () {
        // hide products table
        var $containerData = $("#dataProducts");
        $containerData.empty();
        // hide add form
        $("#formDivTwo").css("display", "none");
        // hide details form
        $("#formDivThree").css("display", "none");
        // hide form for entering price for search
        $("#formDivFour").css("display", "none");
        // show form for chosing category
        $("#formDivFive").css("display", "block");
    });

    // creating second dropdown list for categories
    $.ajax({
        url: host + port + categoriesEndpoint,
        type: "Get"
    })
        .done(function (data, status) {
            var create = "<label>Category: <select id=productCategoryTwo>";
            for (var i = 0; i < data.length; i++) {
                create += "<option value=" + data[i].Id + ">" + data[i].Name + "</option>";
            }
            create += "</select></label>";
            $("#selectionTwo").append(create);
        });

    // Form for chosing category
    $("#productsFromCat").submit(function (e) {

        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        e.preventDefault();

        var categoryId = $("#productCategoryTwo").val();

        $.ajax({
            url: host + port + getByCategoryEndpoint,
            type: "Post",
            data: {
                "Id": categoryId,
                "Name": null
            },
            headers: headers
        })
            .done(function (data, status) {
                setProductsByCat(data, status);
            });
    });

    // setting table for products by category
    function setProductsByCat(data, status) {
        console.log("Status: " + status);

        var $container = $("#dataProductsFromCat");
        $container.empty();

        if (status == "success") {
            console.log(data);

            var div = $("<div></div>");
            var h1 = $("<h3>All products from chosen category</h3>");
            div.append(h1);

            var table = $("<table border=1></table>");
            var header = $("<tr><td>Id</td><td>Name</td><td>Price</td></tr>");
            table.append(header);

            for (z = 0; z < data.length; z++) {
                var row = "<tr>";
                var displayData = "<td>" + data[z].Id + "</td><td>" + data[z].Name + "</td><td>" + data[z].Price + "</td>";
                row += displayData + "</tr>";
                table.append(row);
            }

            div.append(table);

            // clear search form
            $("#productCategoryTwo").val('');

            // printing content
            $container.append(div);
        }
        else {
            var div = $("<div></div>");
            var h1 = $("<h1>Error while reading products from  chosen category!</h1>");
            div.append(h1);
            $container.append(div);
        }
    };
});