﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StorageApi.Models
{
	public class ProductDTO
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}