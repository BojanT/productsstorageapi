﻿using System.ComponentModel.DataAnnotations;

namespace StorageApi.Models
{
	public class Product
	{
		public int Id { get; set; }

		[Required(ErrorMessage = "This field is required")]
		[StringLength(80, ErrorMessage = "Name lenght can't be over 80 characters")]
		public string Name { get; set; }

		[Required(ErrorMessage = "This field is required")]
		[Range(1, double.MaxValue, ErrorMessage = "Value must be higher then 0")]
		public decimal Price { get; set; }

		public int CategoryId { get; set; }

		public Category Category { get; set; }
	}
}