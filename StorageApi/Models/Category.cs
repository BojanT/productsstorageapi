﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace StorageApi.Models
{
	public class Category
	{
		public int Id { get; set; }

		[Required(ErrorMessage = "This field is required")]
		[StringLength(50, ErrorMessage = "Name lenght can't be over 50 characters")]
		public string Name { get; set; }

		public List<Product> Products { get; set; }
	}
}