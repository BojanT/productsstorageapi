﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StorageApi.Models
{
	public class ProductDetailsDTO
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public decimal Price { get; set; }
		public string CategoryName { get; set; }
	}
}