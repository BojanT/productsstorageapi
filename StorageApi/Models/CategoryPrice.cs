﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StorageApi.Models
{
	public class CategoryPrice
	{
		public Category Category { get; set; }
		public decimal PriceSum { get; set; }
	}
}