﻿using System.Web.Http;
using System.Web.Http.Cors;
using Microsoft.Owin.Security.OAuth;
using Unity;
using StorageApi.Models;
using StorageApi.Interfaces;
using StorageApi.Repositories;
using StorageApi.Resolver;
using Unity.Lifetime;
using AutoMapper;


namespace StorageApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

			// CORS configuration
			var cors = new EnableCorsAttribute("*", "*", "*");
			config.EnableCors(cors);

			// Dependency resolver
			var container = new UnityContainer();
			container.RegisterType<IProductRepository, ProductRepository>(new HierarchicalLifetimeManager());
			container.RegisterType<ICategoryRepository, CategoryRepository>(new HierarchicalLifetimeManager());
			config.DependencyResolver = new UnityResolver(container);

			// AutoMapper configuration
			Mapper.Initialize(cfg => {
				cfg.CreateMap<Product, ProductDTO>();
				cfg.CreateMap<Product, ProductDetailsDTO>();
			});

			// Tracing configuration
			config.EnableSystemDiagnosticsTracing();
		}
    }
}
