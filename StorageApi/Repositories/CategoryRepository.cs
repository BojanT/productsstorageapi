﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StorageApi.Models;
using StorageApi.Interfaces;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace StorageApi.Repositories
{
	public class CategoryRepository : IDisposable, ICategoryRepository
	{
		private ApplicationDbContext db = new ApplicationDbContext();

		// Read all categories
		public IEnumerable<Category> GetAll()
		{
			return db.Categories;
		}

		// Read category by id
		public Category GetById(int id)
		{
			try
			{
				var category = db.Categories.FirstOrDefault(x => x.Id == id);

				return category;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		// Read first two categories with highest summed prices 
		public IEnumerable<CategoryPrice> GetStatistics()
		{
			try
			{
				List<CategoryPrice> categoriesDisplay = new List<CategoryPrice>();
				List<CategoryPrice> list = new List<CategoryPrice>();

				var result = db.Products.GroupBy(x => x.Category, x => x.Price, (category, priceSum) => new { Category = category, PriceSum = priceSum.Sum() });
				foreach (var item in result) 
				{
					list.Add(new CategoryPrice() { Category = item.Category, PriceSum = item.PriceSum });
				}

				List<CategoryPrice> sortedList = list.OrderByDescending(x => x.PriceSum).ToList();

				for (int i = 0; i < 2; i++)
				{
					categoriesDisplay.Add(sortedList[i]);
				}

				return categoriesDisplay;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public bool Exists(int id)
		{
			return db.Categories.Count(x => x.Id == id) > 0;
		}

		// Create new category
		public bool Add(Category category)
		{
			try
			{
				if (category == null)
				{
					return false;
				}

				db.Categories.Add(category);
				db.SaveChanges();

				if (!Exists(category.Id))
				{
					return false;
				}

				return true;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		// Update category
		public bool Update(Category category)
		{
			try
			{
				if (category == null)
				{
					return false;
				}

				db.Entry(category).State = EntityState.Modified;

				try
				{
					db.SaveChanges();
				}
				catch (DbUpdateConcurrencyException)
				{
					throw;
				}

				return true;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		// Delete category
		public bool Delete(Category category)
		{
			try
			{
				if (category == null)
				{
					return false;
				}

				db.Categories.Remove(category);
				db.SaveChanges();

				if (Exists(category.Id))
				{
					return false;
				}

				return true;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		protected void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
				db = null;
			}
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}