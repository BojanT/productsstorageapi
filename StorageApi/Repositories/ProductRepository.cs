﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StorageApi.Models;
using StorageApi.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace StorageApi.Repositories
{
	public class ProductRepository : IDisposable, IProductRepository
	{
		private ApplicationDbContext db = new ApplicationDbContext();

		// Read all products
		public IEnumerable<ProductDTO> GetAll()
		{
			return db.Products.ProjectTo<ProductDTO>();
		}

		// Read all products from one category
		public IEnumerable<Product> GetByCategory(Category category)
		{
			return db.Products.Where(x => x.CategoryId == category.Id).ToList();
		}

		// Read all products with lower prices from entered price ascending
		public IEnumerable<Product> GetByPrice(int enteredPrice)
		{
			return db.Products.Where(x => x.Price < enteredPrice).OrderBy(x => x.Price).ToList();
		}

		// Read product details
		public ProductDetailsDTO GetById(int id)
		{
			var product = db.Products.Include(x => x.Category).SingleOrDefault(x => x.Id == id);
			
			return Mapper.Map<ProductDetailsDTO>(product);
		}

		// Show 2 products with lowest price
		public IEnumerable<Product> ShowProductsWithLowestPrices()
		{
			try
			{
				List<Product> productsForDisplay = new List<Product>();
				var products = db.Products.OrderBy(x => x.Price).ToList();

				for (int i = 0; i < 2; i++)
				{
					productsForDisplay.Add(products[i]);
				}

				return productsForDisplay;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public bool Exists(int id)
		{
			return db.Products.Count(x => x.Id == id) > 0;
		}

		// Create new product
		public bool Add(Product product)
		{
			try
			{
				if (product == null)
				{
					return false;
				}

				db.Products.Add(product);
				db.SaveChanges();

				if (!Exists(product.Id))
				{
					return false;
				}

				return true;
			}
			catch (Exception e)
			{
				throw e;
			}

		}

		// Update product
		public bool Update(Product product)
		{
			try
			{
				if (product == null)
				{
					return false;
				}

				db.Entry(product).State = EntityState.Modified;

				try
				{
					db.SaveChanges();
				}
				catch (DbUpdateConcurrencyException)
				{
					throw;
				}

				return true;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		// Delete product
		public bool Delete(int id)
		{
			try
			{
				var product = db.Products.Find(id);

				if (product == null)
				{
					return false;
				}

				db.Products.Remove(product);

				db.SaveChanges();

				return true;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		protected void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
				db = null;
			}
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}