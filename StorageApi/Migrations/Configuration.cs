namespace StorageApi.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
	using StorageApi.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<StorageApi.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(StorageApi.Models.ApplicationDbContext context)
        {
			context.Categories.AddOrUpdate(x => x.Id,
				new Category() { Id = 1, Name = "Shoes" },
				new Category() { Id = 2, Name = "Textile" },
				new Category() { Id = 3, Name = "Equipment" });

			context.Products.AddOrUpdate(x => x.Id,
				new Product() { Id = 1, Name = "Jacket", Price = 850, CategoryId = 2 },
				new Product() { Id = 2, Name = "Pants", Price = 450, CategoryId = 2 },
				new Product() { Id = 3, Name = "Bicycle", Price = 1250, CategoryId = 3 },
				new Product() { Id = 4, Name = "Backpack", Price = 380, CategoryId = 3 },
				new Product() { Id = 5, Name = "Trail shoes", Price = 590, CategoryId = 1 },
				new Product() { Id = 6, Name = "Tennis shoes", Price = 590, CategoryId = 1 });
        }
    }
}
